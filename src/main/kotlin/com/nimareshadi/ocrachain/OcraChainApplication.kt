package com.nimareshadi.ocrachain

import com.nimareshadi.ocrachain.config.ResourcePropertyConfiguration
import com.nimareshadi.ocrachain.config.WebClientConfiguration
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.MessageSource
import org.springframework.context.annotation.Bean
import org.springframework.context.support.ReloadableResourceBundleMessageSource
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean


@SpringBootApplication
@EnableConfigurationProperties(ResourcePropertyConfiguration::class)
class OcraChainApplication{

    @Bean
    fun messageSource(): MessageSource {
        val messageSource = ReloadableResourceBundleMessageSource()
        messageSource.setBasename("classpath:messages")
        messageSource.setDefaultEncoding("UTF-8")
        return messageSource
    }

    @Bean
    fun getValidator(): LocalValidatorFactoryBean? {
        val bean = LocalValidatorFactoryBean()
        bean.setValidationMessageSource(messageSource())
        return bean
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(OcraChainApplication::class.java, *args)

}
