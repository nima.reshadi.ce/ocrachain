package com.nimareshadi.ocrachain.common

import javax.validation.*

abstract class SelfValidating<T> {
    private val validator: Validator
    init {
        val factory = Validation.buildDefaultValidatorFactory()
        validator = factory.validator
    }
    protected fun validateSelf() {
        val violations = validator.validate(this )
        if (violations.isNotEmpty()) {
            throw ConstraintViolationException(violations)
        }
    }
}