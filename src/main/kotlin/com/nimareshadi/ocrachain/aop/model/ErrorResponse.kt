package com.nimareshadi.ocrachain.aop.model

import io.swagger.v3.oas.annotations.media.Schema

data class ErrorResponse(
    @field:Schema(description = "Error message")
    var message: String,
    @field:Schema(description = "Error message details")
    var details: ArrayList<String?>
)