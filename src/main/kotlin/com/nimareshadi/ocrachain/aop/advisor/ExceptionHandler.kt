package com.nimareshadi.ocrachain.aop.advisor

import com.nimareshadi.ocrachain.aop.factory.ErrorResponseFactory
import com.nimareshadi.ocrachain.aop.model.ErrorResponse
import org.springframework.context.MessageSource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import org.webjars.NotFoundException
import java.util.*
import javax.naming.ServiceUnavailableException

import javax.validation.ConstraintViolationException


@ControllerAdvice
class ExceptionHandler(val messageSource: MessageSource, val errorResponseFactory: ErrorResponseFactory) :
    ResponseEntityExceptionHandler() {
    override fun handleMethodArgumentNotValid(
        ex: MethodArgumentNotValidException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        super.handleMethodArgumentNotValid(ex, headers, status, request)
        val details = ArrayList<String?>()
        for (error in ex.bindingResult.allErrors) {
            details.add(error.defaultMessage)
        }
        return ResponseEntity(
            errorResponseFactory.create(
                messageSource.getMessage(
                    "validation.failed.error",
                    null, Locale.ENGLISH
                ), details
            ), HttpStatus.BAD_REQUEST
        )
    }

    @ExceptionHandler(ConstraintViolationException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleCVExceptions(ex: ConstraintViolationException): ResponseEntity<ErrorResponse?> {
        val details = ArrayList<String?>()
        for (error in ex.constraintViolations) {
            details.add(messageSource.getMessage(error.message, null, Locale.ENGLISH))
        }
        return ResponseEntity(
            errorResponseFactory.create(
                messageSource.getMessage(
                    "validation.failed.error",
                    null, Locale.ENGLISH
                ), details
            ), HttpStatus.BAD_REQUEST
        )
    }

    @ExceptionHandler(ServiceUnavailableException::class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    fun handleServiceUnavailableException(ex: ServiceUnavailableException): ResponseEntity<ErrorResponse?> {
        return ResponseEntity(
            errorResponseFactory.create(ex.localizedMessage, ArrayList<String?>()),
            HttpStatus.SERVICE_UNAVAILABLE
        )

    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException::class)
    fun handleNotFoundException(ex: NotFoundException): ResponseEntity<ErrorResponse?> {
        return ResponseEntity(
            errorResponseFactory.create(ex.localizedMessage, ArrayList<String?>()),
            HttpStatus.NOT_FOUND
        )
    }
}