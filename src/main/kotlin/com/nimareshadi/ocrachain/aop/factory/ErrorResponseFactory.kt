package com.nimareshadi.ocrachain.aop.factory

import com.nimareshadi.ocrachain.aop.model.ErrorResponse
import org.springframework.stereotype.Component

@Component
class ErrorResponseFactory {
    fun create(message: String, details: ArrayList<String?>): ErrorResponse {
        return ErrorResponse(message, details)
    }
}