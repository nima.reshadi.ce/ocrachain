package com.nimareshadi.ocrachain.flight.domain.model

class Flight() {
    var greatCircleDistance: GreatCircleDistance? = null
    var departure: Departure? = null
    var arrival: Arrival? = null
    var lastUpdatedUtc : String? = ""
    var number : String? = ""
    var callSign : String? = ""
    var status : String? = ""
    var codeshareStatus : String? = ""
    var isCargo : Boolean = false
    var aircraft : Aircraft? = null
    var airline : Airline? = null
    var location: Location? = null
}


data class GreatCircleDistance(
    var meter : Double? = 0.0,
    var km : Double? = 0.0,
    var mile : Double? = 0.0,
    var nm : Double? = 0.0,
    var feet : Double? = 0.0
)
data class Departure(
    var airport : Airport?,
    var scheduledTimeLocal : String? = "",
    var actualTimeLocal : String? = "",
    var runwayTimeLocal : String? = "",
    var scheduledTimeUtc : String? = "",
    var actualTimeUtc : String? = "",
    var runwayTimeUtc : String? = "",
    var terminal : String? = "",
    var checkInDesk : String? = "",
    var gate : String? = "",
    var baggageBelt : String? = "",
    var runway : String? = "",
    var quality : List<String?>? = null
)
data class Arrival(
    var airport : Airport?,
    var scheduledTimeLocal : String? = "",
    var actualTimeLocal : String? = "",
    var runwayTimeLocal : String? = "",
    var scheduledTimeUtc : String? = "",
    var actualTimeUtc : String? = "",
    var runwayTimeUtc : String? = "",
    var terminal : String? = "",
    var checkInDesk : String? = "",
    var gate : String? = "",
    var baggageBelt : String? = "",
    var runway : String? = "",
    var quality : List<String?>? = null
)
class Airport{
    var icao : String? = ""
    var iata : String? = ""
    var name : String? = ""
    var location : Location? = null
    var shortName : String? = ""
    var municipalityName : String? = ""

    class Location{
        var lat : Float = 0f
        var lon : Float = 0f
    }
}
data class Aircraft(
    var reg : String? = "",
    var modeS : String? = "",
    var model : String? = "",
    var image : Image?
)
data class Image(
    var url : String? = "",
    var webUrl : String? = "",
    var author : String? = "",
    var title : String? = "",
    var description : String? = "",
    var license : String? = "",
    var htmlAttributions : List<String?>?
)
data class Airline(
    var name : String? = "",
)
data class Location(
    var pressureAltFt : Int? = 0,
    var gsKt : Int? = 0,
    var trackDeg : Int? = 0,
    var reportedAtUtc : String? = "",
    var lat : Float? = 0f,
    var lon : Float? = 0f,
)