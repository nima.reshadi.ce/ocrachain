package com.nimareshadi.ocrachain.flight.application.port.`in`.usecase

import com.nimareshadi.ocrachain.common.UseCase
import com.nimareshadi.ocrachain.flight.application.port.`in`.model.FlightCommand
import com.nimareshadi.ocrachain.flight.domain.model.Flight
import org.springframework.http.ResponseEntity

@UseCase
interface LoadFlightUseCase {
    fun invoke(flightCommand: FlightCommand) : ResponseEntity<List<Flight>>?
}