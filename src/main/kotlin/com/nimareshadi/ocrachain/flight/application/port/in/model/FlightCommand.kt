package com.nimareshadi.ocrachain.flight.application.port.`in`.model
import com.nimareshadi.ocrachain.common.SelfValidating
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

data class FlightCommand(
    @field:NotEmpty(message = "flight.number.empty.error") var flightNumber: String,
    @field:NotBlank(message = "flight.date.empty.error") var flightDate: String
) : SelfValidating<FlightCommand>() {
    init {
        this.validateSelf()
    }
}








