package com.nimareshadi.ocrachain.flight.application.service

import com.nimareshadi.ocrachain.flight.application.port.`in`.model.FlightCommand
import com.nimareshadi.ocrachain.flight.application.port.`in`.usecase.LoadFlightUseCase
import com.nimareshadi.ocrachain.flight.application.port.out.usecase.LoadFlightFromProviderUseCase
import com.nimareshadi.ocrachain.flight.domain.model.Flight
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class LoadFlightService(val loadFlightFromProviderUseCase: LoadFlightFromProviderUseCase) : LoadFlightUseCase{
     override fun invoke(flightCommand: FlightCommand): ResponseEntity<List<Flight>>?  {
        return loadFlightFromProviderUseCase.load(flightCommand)
    }
}