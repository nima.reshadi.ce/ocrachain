package com.nimareshadi.ocrachain.flight.application.port.`in`.factory

import com.nimareshadi.ocrachain.flight.application.port.`in`.model.FlightCommand
import org.springframework.stereotype.Component
@Component
class FlightCommandFactory {
    fun createInstance(number : String,date : String) : FlightCommand{
        return FlightCommand(number,date)
    }
}