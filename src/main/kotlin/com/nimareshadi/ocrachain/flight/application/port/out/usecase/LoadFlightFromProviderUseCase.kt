package com.nimareshadi.ocrachain.flight.application.port.out.usecase

import com.nimareshadi.ocrachain.common.UseCase
import com.nimareshadi.ocrachain.flight.application.port.`in`.model.FlightCommand
import com.nimareshadi.ocrachain.flight.domain.model.Flight
import org.springframework.http.ResponseEntity

@UseCase
interface LoadFlightFromProviderUseCase {
    fun load(flightCommand: FlightCommand) : ResponseEntity<List<Flight>>?
}