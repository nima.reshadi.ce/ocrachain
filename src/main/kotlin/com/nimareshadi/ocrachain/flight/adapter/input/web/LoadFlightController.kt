package com.nimareshadi.ocrachain.flight.adapter.input.web

import com.nimareshadi.ocrachain.flight.application.port.`in`.factory.FlightCommandFactory
import com.nimareshadi.ocrachain.flight.application.port.`in`.usecase.LoadFlightUseCase
import com.nimareshadi.ocrachain.flight.domain.model.Flight
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/flight")
@Tag(name = "flight", description = "The flight API")
class LoadFlightController(val loadFlightUseCase: LoadFlightUseCase, val flightCommandFactory: FlightCommandFactory) {

    @GetMapping
    @Operation(summary = "Get flight information", description = "Gets information about the status of the nearest" +
            " (either in past or in future) flight or about flight departing or arriving on the day specified " +
            "(local time), operated under specified flight number")

    fun load(@Parameter(description="Flight number") @RequestParam number: String,
             @Parameter(description="Flight date") @RequestParam date: String): ResponseEntity<List<Flight>>? {
        return loadFlightUseCase.invoke(flightCommandFactory.createInstance(number, date)
        )
    }
}