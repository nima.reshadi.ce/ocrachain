package com.nimareshadi.ocrachain.flight.adapter.output.web

import com.nimareshadi.ocrachain.flight.application.port.`in`.model.FlightCommand
import com.nimareshadi.ocrachain.flight.application.port.out.usecase.LoadFlightFromProviderUseCase
import com.nimareshadi.ocrachain.flight.domain.model.Flight
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.toEntityList
import org.webjars.NotFoundException
import reactor.core.publisher.Mono
import java.util.*
import javax.naming.ServiceUnavailableException

@Component
class FlightProvider(@Qualifier("flightWebClient") val webClient: WebClient,
                     val messageSource: MessageSource)  : LoadFlightFromProviderUseCase{
    override fun load(flightCommand: FlightCommand): ResponseEntity<List<Flight>>? {
        return  webClient
            .get()
            .uri("/flights/number/${flightCommand.flightNumber}/${flightCommand.flightDate}")
            .retrieve()
            .onStatus(HttpStatus.NOT_FOUND::equals){
                Mono.error(NotFoundException(
                    messageSource.getMessage("no.such.flight.found",null,Locale.ENGLISH)))}
            .onStatus(HttpStatus.BAD_REQUEST::equals){
                Mono.error(NotFoundException(
                    messageSource.getMessage("no.such.flight.found",null,Locale.ENGLISH)))}
            .onStatus(HttpStatus.UNAUTHORIZED::equals) {
                Mono.error(ServiceUnavailableException(
                    messageSource.getMessage("server.provider.unavailable.error",null,Locale.ENGLISH))) }
            .onStatus(HttpStatus::is5xxServerError) {
                Mono.error(ServiceUnavailableException(
                    messageSource.getMessage("server.provider.unavailable.error",null, Locale.ENGLISH))) }
            .toEntityList<Flight>()
            .block()
    }

}
