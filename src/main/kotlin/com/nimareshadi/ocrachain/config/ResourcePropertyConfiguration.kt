package com.nimareshadi.ocrachain.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.validation.annotation.Validated


@ConfigurationProperties(prefix = "app")
@Validated
data class ResourcePropertyConfiguration @ConstructorBinding constructor(
    val flight : Flight
)

data class Flight(
    val base_url: String,
    val host_header: String ,
    val key_header: String ,
    val connectingTimeOut: Int ,
    val onConnectedTimeOut: Long,
    val responseTimeOut: Long
)


