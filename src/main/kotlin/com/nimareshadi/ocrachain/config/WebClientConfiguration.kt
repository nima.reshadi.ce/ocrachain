package com.nimareshadi.ocrachain.config


import io.netty.channel.ChannelOption
import io.netty.handler.timeout.ReadTimeoutHandler
import io.netty.handler.timeout.WriteTimeoutHandler
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.web.reactive.config.WebFluxConfigurer
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.Connection
import reactor.netty.http.client.HttpClient
import java.time.Duration
import java.util.concurrent.TimeUnit


@Configuration
class WebClientConfiguration(val resourcePropertyConfiguration: ResourcePropertyConfiguration) : WebFluxConfigurer{

    @Qualifier("flightWebClient")
    @Bean
    fun flightWebClient(): WebClient {
        val httpClient = HttpClient.create()
            .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, resourcePropertyConfiguration.flight.connectingTimeOut)
            .responseTimeout(Duration.ofMillis(resourcePropertyConfiguration.flight.responseTimeOut))
            .doOnConnected { conn: Connection ->
                conn.addHandlerLast(ReadTimeoutHandler(resourcePropertyConfiguration.flight.onConnectedTimeOut, TimeUnit.MILLISECONDS))
                    .addHandlerLast(WriteTimeoutHandler(resourcePropertyConfiguration.flight.onConnectedTimeOut, TimeUnit.MILLISECONDS))
            }
        return WebClient.builder()
            .baseUrl(resourcePropertyConfiguration.flight.base_url)
            .defaultHeader("x-rapidapi-host",resourcePropertyConfiguration.flight.host_header)
            .defaultHeader("x-rapidapi-key",resourcePropertyConfiguration.flight.key_header)
            .clientConnector(ReactorClientHttpConnector(httpClient))
            .build()
    }
}

